# 临境视频播放器
#### 根据官方文档编写的，提供沉浸式视频播放体验。

##### 支持 visionOS, iOS, tvOS.
##### 需要环境 Xcode 15.2 以上。

![image](https://github.com/YiLee01/picx-images-hosting/raw/master/20240229164725.1lbk6gm369.webp)
![image](https://github.com/YiLee01/picx-images-hosting/raw/master/20240229164715.5c0prpaydk.webp)
![image](https://github.com/YiLee01/picx-images-hosting/raw/master/20240229164645.5j3xn4x3sw.webp)
