//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import AVFoundation

// 从 AVAudioSession 中断事件中解压相关值
struct InterruptionResult {
    
    let type: AVAudioSession.InterruptionType
    let options: AVAudioSession.InterruptionOptions
    
    init?(_ notification: Notification) {
        // 确定中断类型和选项
        guard let type = notification.userInfo?[AVAudioSessionInterruptionTypeKey] as? AVAudioSession.InterruptionType,
              let options = notification.userInfo?[AVAudioSessionInterruptionOptionKey] as? AVAudioSession.InterruptionOptions else {
            return nil
        }
        self.type = type
        self.options = options
    }
}
