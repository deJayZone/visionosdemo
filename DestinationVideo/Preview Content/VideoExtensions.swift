import Foundation

extension Video {
    static var preview: Video {
        VideoLibrary().videos[0]
    }
}

extension Array {
    static var all: [Video] {
        VideoLibrary().videos
    }
}

