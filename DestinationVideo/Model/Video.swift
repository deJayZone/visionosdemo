//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import Foundation
import UIKit

struct Video: Identifiable, Hashable, Codable {
    
    let id: Int
    let url: URL
    let title: String
    let imageName: String
    let description: String
    var portraitImageName: String { "\(imageName)_portrait" }
    var landscapeImageName: String { "\(imageName)_landscape" }
    var imageData: Data {
        UIImage(named: landscapeImageName)?.pngData() ?? Data()
    }
    let info: Info
    var resolvedURL: URL {
        if url.scheme == nil {
            return URL(fileURLWithPath: "\(Bundle.main.bundlePath)/\(url.path)")
        }
        return url
    }
    
    var hasRemoteMedia: Bool {
        url.scheme != nil
    }
    
    /// 观看视频的地点
    /// 在沉浸式空间中呈现
    var destination: Destination
    
    struct Info: Hashable, Codable {
        var releaseYear: String
        var contentRating: String
        var duration: String
        var genres: [String]
        var stars: [String]
        var directors: [String]
        var writers: [String]
        
        var releaseDate: Date {
            var components = DateComponents()
            components.year = Int(releaseYear)
            let calendar = Calendar(identifier: .gregorian)
            return calendar.date(from: components)!
        }
    }
}
