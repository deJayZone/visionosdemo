//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import Foundation

enum Destination: String, CaseIterable, Identifiable, Codable {
    
    case beach
    case camping
    case creek
    case hillside
    case lake
    case ocean
    case park
    
    var id: Self { self }
    
    /// 要加载的环境图片
    var imageName: String { "\(rawValue)_scene" }
    
    /// 为提供最佳初始视图，需要旋转360度"目的地"图片的角度数
    var rotationDegrees: Double {
        switch self {
        case .beach: 55
        case .camping: -55
        case .creek: 0
        case .hillside: 0
        case .lake: -55
        case .ocean: 0
        case .park: 190
        }
    }
}
