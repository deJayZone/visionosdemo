//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import Foundation
import SwiftUI
import Observation

/// 管理应用视频内容
///
/// 应用将此类的一个实例放入环境中，以便它可以检索和更新图书馆中视频内容的状态。
@Observable class VideoLibrary {
    
    private(set) var videos = [Video]()
    private(set) var upNext = [Video]()
    
    // 用户的Documents目录下的URL，用于写入他们的"即将播放"条目
    private let upNextURL = URL.documentsDirectory.appendingPathComponent("UpNext.json")
    
    init() {
        // 加载图书馆中所有可用的视频
        videos = loadVideos()
        // 应用第一次启动时，将最后三个视频设置为默认的"即将播放"项
        upNext = loadUpNextVideos(default: Array(videos.suffix(3)))
    }
    
    /// 切换视频是否存在于"即将播放"队列中
    /// - Parameter video: 要更新的视频
    func toggleUpNextState(for video: Video) {
        if !upNext.contains(video) {
            // 在列表的开头插入视频
            upNext.insert(video, at: 0)
        } else {
            // 删除具有匹配标识符的条目
            upNext.removeAll(where: { $0.id == video.id })
        }
        // 将"即将播放"状态持久化到磁盘
        saveUpNext()
    }
    
    /// 返回一个布尔值，指示视频是否存在于"即将播放"列表中
    /// - Parameter video: 要测试的视频,
    /// - Returns: `true` 如果该项在即将播放列表中; 否则`false`
    func isVideoInUpNext(_ video: Video) -> Bool {
        upNext.contains(video)
    }
    
    /// 找到要在视频播放器的"即将播放"列表中显示的项
    func findUpNext(for video: Video) -> [Video] {
        upNext.filter { $0.id != video.id }
    }
    
    /// 找到当前视频之后，在"即将播放"列表中的下一个视频
    /// - Parameter video: 当前视频
    /// - Returns: 下一个视频，如果不存在，返回`nil`
    func findVideoInUpNext(after video: Video) -> Video? {
        switch upNext.count {
        case 0:
            // 即将播放为空
            return nil
        case 1:
            // 传入的视频是"即将播放"中的唯一项目
            if upNext.first == video {
                return nil
            } else {
                // 返回唯一的项目
                return upNext.first
            }
        default:
            // 找到传入视频的索引。如果视频不在"即将播放"中，从第一项开始
            let videoIndex = upNext.firstIndex(of: video) ?? 0
            if videoIndex < upNext.count - 1 {
                return upNext[videoIndex + 1]
            }
            return upNext[0]
        }
    }
    
    /// 加载应用的视频内容
    private func loadVideos() -> [Video] {
        let filename = "Videos.json"
        guard let url = Bundle.main.url(forResource: filename, withExtension: nil) else {
            fatalError("Couldn't find \(filename) in main bundle.")
        }
        return load(url)
    }
    
    /// 加载用户在"即将播放"列表中的视频
    private func loadUpNextVideos(`default` defaultVideos: [Video]) -> [Video] {
        // 如果这个文件不存在，创建它
        if !FileManager.default.fileExists(atPath: upNextURL.path) {
            // 使用默认值创建一个初始文件
            if !FileManager.default.createFile(atPath: upNextURL.path, contents: "\(defaultVideos.map { $0.id })".data(using: .utf8)) {
                fatalError("Couldn't initialize Up Next store.")
            }
        }
        // 加载列表中视频的id
        let ids: [Int] = load(upNextURL)
        return videos.filter { ids.contains($0.id) }
    }
    
    /// 将"即将播放"数据保存到磁盘
    ///
    /// 应用使用简单的JSON持久性保存状态
    private func saveUpNext() {
        do {
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            // 只持久化id
            let data = try encoder.encode(upNext.map { $0.id })
            try data.write(to: upNextURL)
        } catch {
            logger.error("Unable to save JSON data.")
        }
    }
    
    private func load<T: Decodable>(_ url: URL, as type: T.Type = T.self) -> T {
        let data: Data
        do {
            data = try Data(contentsOf: url)
        } catch {
            fatalError("Couldn't load \(url.path):\n\(error)")
        }
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            return try decoder.decode(T.self, from: data)
        } catch {
            fatalError("Couldn't parse \(url.lastPathComponent) as \(T.self):\n\(error)")
        }
    }
}
