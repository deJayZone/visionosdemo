//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    /// 视频库的选择路径
    @State private var navigationPath = [Video]()
    /// 是否正在展示一个沉浸式空间
    @State private var isPresentingSpace = false
    /// 播放器模型
    @Environment(PlayerModel.self) private var player
    
    var body: some View {
        #if os(visionOS)
        switch player.presentation {
        case .fullWindow:
            // 全屏展示播放器并开始播放
            PlayerView()
                .onAppear {
                    player.play()
                }
        default:
            // 默认情况下显示应用的内容库
            LibraryView(path: $navigationPath, isPresentingSpace: $isPresentingSpace)
        }
        #else
        LibraryView(path: $navigationPath)
            // 在iOS和tvOS中全屏模式展示播放器
            .fullScreenCoverPlayer(player: player)
        #endif
    }
}

#Preview {
    ContentView()
        .environment(PlayerModel())
        .environment(VideoLibrary())
}
