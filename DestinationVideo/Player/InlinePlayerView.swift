//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import AVKit
import SwiftUI

struct InlinePlayerView: View {
    
    @Environment(PlayerModel.self) private var model
    
    var body: some View {
        ZStack {
            // 使用 AVPlayerViewControlle r显示视频内容但不带控件的视图
            VideoContentView()
            // 在视频内容上覆盖自定义内联控制器
            InlineControlsView()
        }
        .onDisappear {
            // 如果这个视图消失了，并且不是因为切换到全窗口展示，则清除模型的加载媒体
            if model.presentation != .fullWindow {
                model.reset()
            }
        }
    }
}

/// 定义简单的播放/暂停/重播按钮的视图，用于预告片播放器
struct InlineControlsView: View {
    
    @Environment(PlayerModel.self) private var player
    @State private var isShowingControls = false
    
    var body: some View {
        VStack {
            Image(systemName: player.isPlaying ? "pause.fill" : "play.fill")
                .padding(8)
                .background(.thinMaterial)
                .clipShape(.circle)
                    
        }
        .font(.largeTitle)
        .frame(maxWidth: .infinity, maxHeight: .infinity)
        .contentShape(Rectangle())
        .onTapGesture {
            player.togglePlayback()
            isShowingControls = true
            // 在下一个运行循环周期中执行以下代码
            Task { @MainActor in
                if player.isPlaying {
                    dismissAfterDelay()
                }
            }
        }
    }
    
    func dismissAfterDelay() {
        Task {
            try! await Task.sleep(for: .seconds(3.0))
            withAnimation(.easeOut(duration: 0.3)) {
                isShowingControls = false
            }
        }
    }
}

/// 展示播放器对象的视频内容的视图。
///
/// 这个类是一个视图控制器可表示的类型，它适应了 AVPlayerViewController 的接口。
/// 它禁用了视图控制器的默认控件，所以它可以在视频内容上绘制自定义控件。
private struct VideoContentView: UIViewControllerRepresentable {
    
    @Environment(PlayerModel.self) private var model
    
    func makeUIViewController(context: Context) -> AVPlayerViewController {
        let controller = model.makePlayerViewController()
        // 禁用默认的系统播放控件
        controller.showsPlaybackControls = false
        return controller
    }
    
    func updateUIViewController(_ uiViewController: AVPlayerViewController, context: Context) {}
}

#Preview {
    InlineControlsView()
        .environment(PlayerModel())
}
