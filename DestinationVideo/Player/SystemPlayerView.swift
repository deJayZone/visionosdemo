//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import AVKit
import SwiftUI

//  这个视图是对 AVPlayerViewController 的 SwiftUI 包装
struct SystemPlayerView: UIViewControllerRepresentable {

    @Environment(PlayerModel.self) private var model
    @Environment(VideoLibrary.self) private var library
    
    let showContextualActions: Bool
    
    init(showContextualActions: Bool) {
        self.showContextualActions = showContextualActions
    }
    
    func makeUIViewController(context: Context) -> AVPlayerViewController {
        
        let controller = model.makePlayerViewController()
        controller.allowsPictureInPicturePlayback = true

        #if os(visionOS) || os(tvOS)
        // 显示即将播放的标签
        if let upNextViewController {
            controller.customInfoViewControllers = [upNextViewController]
        }
        #endif
        
        return controller
    }
    
    func updateUIViewController(_ controller: AVPlayerViewController, context: Context) {
        #if os(visionOS) || os(tvOS)
        Task { @MainActor in
            // 重新创建相关视频列表
            if let upNextViewController {
                controller.customInfoViewControllers = [upNextViewController]
            }
            
            if let upNextAction, showContextualActions {
                controller.contextualActions = [upNextAction]
            } else {
                controller.contextualActions = []
            }
        }
        #endif
    }
    
    // 即将播放视频列表的视图控制器
    var upNextViewController: UIViewController? {
        guard let video = model.currentItem else { return nil }
        
        // 查找此视频的即将播放列表，如果没有则提前返回。
        let videos = library.findUpNext(for: video)
        if videos.isEmpty { return nil }

        let view = UpNextView(videos: videos, model: model)
        let controller = UIHostingController(rootView: view)
        // AVPlayerViewController 使用试图控制器的title作为标签名称
        // 在把视图控制器设置为 `customInfoViewControllers` 值之前，先设置视图控制器的title
        controller.title = view.title
        // 为标签设置内容大小
        controller.preferredContentSize = CGSize(width: 500, height: isTV ? 250 : 150)
        
        return controller
    }
    
    var upNextAction: UIAction? {
        // 如果没有加载视频，则返回 nil
        guard let video = model.currentItem else { return nil }

        // 找到下一个播放的视频
        guard let nextVideo = library.findVideoInUpNext(after: video) else { return nil }
        
        return UIAction(title: "播放下一个", image: UIImage(systemName: "play.fill")) { _ in
            // 为全屏展示加载视频
            model.loadVideo(nextVideo, presentation: .fullWindow)
        }
    }
}
