//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import SwiftUI

/// 定义播放器展示的控制器样式的常量
enum PlayerControlsStyle {
    /// 表示使用AVPlayerViewController提供的系统界面
    case system
    /// 表示使用展示播放/暂停按钮的紧凑型控制器
    case custom
}

/// 展示视频播放器的视图
struct PlayerView: View {
    
    let controlsStyle: PlayerControlsStyle
    @State private var showContextualActions = false
    @Environment(PlayerModel.self) private var model
    
    /// 创建一个新的播放器视图
    init(controlsStyle: PlayerControlsStyle = .system) {
        self.controlsStyle = controlsStyle
    }
    
    var body: some View {
        switch controlsStyle {
        case .system:
            SystemPlayerView(showContextualActions: showContextualActions)
                .onChange(of: model.shouldProposeNextVideo) { oldValue, newValue in
                    if oldValue != newValue {
                        showContextualActions = newValue
                    }
                }
        case .custom:
            InlinePlayerView()
        }
    }
}
