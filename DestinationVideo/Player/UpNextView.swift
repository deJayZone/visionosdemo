//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import SwiftUI

struct UpNextView: View {
    
    let title = "即将播放"
    
    let videos: [Video]
    let model: PlayerModel
    
    var body: some View {
        VideoListView(videos: videos, cardStyle: .compact, cardSpacing: isTV ? 50 : 30) { video in
            model.loadVideo(video, presentation: .fullWindow)
        }
    }
}

#Preview {
    UpNextView(videos: .all, model: PlayerModel())
        .padding()
}
