//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import AVKit
import GroupActivities
import Combine
import Observation

/// 针对播放器支持的展示模式
enum Presentation {
    /// 表示将播放器作为父用户界面的子项进行呈现
    case inline
    /// 表示在全窗口独占模式中呈现播放器
    case fullWindow
}

@Observable class PlayerModel {
    
    /// 表示当前处于活动播放状态
    private(set) var isPlaying = false
    
    /// 表示当前项的播放是否完成
    private(set) var isPlaybackComplete = false
    
    /// 显示当前媒体的演示文稿
    private(set) var presentation: Presentation = .inline
    
    /// 当前已载入的视频
    private(set) var currentItem: Video? = nil
    
    /// 表示播放器是否应建议在"下一个视频"列表中播放下一个视频
    private(set) var shouldProposeNextVideo = false
    
    private var player = AVPlayer()
    
    /// 当前呈现的播放器视图控制器
    ///
    /// `AVPlayerViewController`对象的生命周期与典型的视图控制器不同。除了在您的应用程序中显示播放器用户界面外，视图控制器还管理媒体的演示文稿
    /// 位于您的应用程序用户界面之外，例如在使用AirPlay，画中画或停靠全窗口时。为确保在这些情况下保留视图控制器实例，应用程序将其存储在此处(环境范围对象)
    ///
    /// 通过调用`makePlayerViewController()`方法设置此值
    private var playerViewController: AVPlayerViewController? = nil
    private var playerViewControllerDelegate: AVPlayerViewControllerDelegate? = nil
    
    private(set) var shouldAutoPlay = true
    
    // 管理应用程序的SharePlay实现的对象
    private var coordinator: VideoWatchingCoordinator! = nil
    
    /// 播放器时间的定期观察令牌
    private var timeObserver: Any? = nil
    private var subscriptions = Set<AnyCancellable>()
    
    init() {
        coordinator = VideoWatchingCoordinator(playbackCoordinator: player.playbackCoordinator)
        observePlayback()
        Task {
            await configureAudioSession()
            await observeSharedVideo()
        }
    }
    
    /// 创建一个新的播放器视图控制器
    /// - Returns: 一个已配置的播放器视图控制器
    func makePlayerViewController() -> AVPlayerViewController {
        let delegate = PlayerViewControllerDelegate(player: self)
        let controller = AVPlayerViewController()
        controller.player = player
        controller.delegate = delegate

        // 设置模型状态
        playerViewController = controller
        playerViewControllerDelegate = delegate
        
        return controller
    }
    
    private func observePlayback() {
        // 模型多次调用此方法则return
        guard subscriptions.isEmpty else { return }
        
        // 观察时间控制状态以确定是否正在进行播放
        player.publisher(for: \.timeControlStatus)
            .receive(on: DispatchQueue.main)
            .removeDuplicates()
            .sink { [weak self] status in
                self?.isPlaying = status == .playing
            }
            .store(in: &subscriptions)
        
        // 观察此通知以知道视频何时播放结束
        NotificationCenter.default
            .publisher(for: .AVPlayerItemDidPlayToEndTime)
            .receive(on: DispatchQueue.main)
            .map { _ in true }
            .sink { [weak self] isPlaybackComplete in
                self?.isPlaybackComplete = isPlaybackComplete
            }
            .store(in: &subscriptions)
        
        // 观察音频会话中断
        NotificationCenter.default
            .publisher(for: AVAudioSession.interruptionNotification)
            .receive(on: DispatchQueue.main)
            .sink { [weak self] notification in
                
                // 将通知封装在帮助类型中，提取中断类型和选项
                guard let result = InterruptionResult(notification) else { return }
                
                // 恢复播放
                if result.type == .ended && result.options == .shouldResume {
                    self?.player.play()
                }
            }.store(in: &subscriptions)
        
        // 添加播放器对象的当前时间的观察者。应用程序观察播放器的当前时间以确定何时提出在接下来的列表中播放下一个视频
        addTimeObserver()
    }
    
    /// 为视频播放配置音频会话
    private func configureAudioSession() async {
        let session = AVAudioSession.sharedInstance()
        do {
            // 音频会话配置为播放。将 `moviePlayback` 模式设置为减少音频的动态范围，以帮助规范音频水平
            try session.setCategory(.playback, mode: .moviePlayback)
        } catch {
            logger.error("无法配置音频会话: \(error.localizedDescription)")
        }
    }

    /// 监视协调器的 `sharedVideo` 属性，如果由于远程参与者共享新的活动而更改此值，请加载并显示新的视频
    private func observeSharedVideo() async {
        let current = currentItem
        await coordinator.$sharedVideo
            .receive(on: DispatchQueue.main)
            // 只观察非零值
            .compactMap { $0 }
            // 只观察由远程参与者设置的更新
            .filter { $0 != current }
            .sink { [weak self] video in
                guard let self else { return }
                // 为全窗口演示加载视频
                loadVideo(video, presentation: .fullWindow)
            }
            .store(in: &subscriptions)
    }
    
    /// 为所请求的演示加载视频以进行播放
    /// - 参数:
    ///   - video: 要加载以供播放的视频
    ///   - presentation: 呈现播放器的样式
    ///   - autoplay: 指示在呈现时是否自动播放内容
    func loadVideo(_ video: Video, presentation: Presentation = .inline, autoplay: Bool = true) {
        // 根据请求更新模型的状态
        currentItem = video
        shouldAutoPlay = autoplay
        isPlaybackComplete = false
        
        switch presentation {
        case .fullWindow:
            Task { @MainActor in
                // 如果FaceTime调用处于活动状态，则尝试 SharePlay 此视频。
                await coordinator.coordinatePlayback(of: video)
                // 将视频加载到播放器中并播放
                replaceCurrentItem(with: video)
            }
        case .inline:
            // 当从内联播放器播放视频时，不 SharePlay 视频，而是将视频加载到播放器中并播放。
            replaceCurrentItem(with: video)
        }

        // 在 visionOS 中，为 .inline 或 .fullWindow 播放配置空间体验
        configureAudioExperience(for: presentation)

        // 设置演示文稿，它通常以全窗口的形式演示播放器
        self.presentation = presentation
   }
    
    private func replaceCurrentItem(with video: Video) {
        // 创建一个新的播放器项目，并将其设置为播放器的当前项目。
        let playerItem = AVPlayerItem(url: video.resolvedURL)
        // 将当前视频的外部元数据设置在播放器项目上。
        playerItem.externalMetadata = createMetadataItems(for: video)
        // 将新的播放器项目设置为当前的，并开始加载其数据。
        player.replaceCurrentItem(with: playerItem)
        logger.debug("🍿 \(video.title) enqueued for playback.")
    }

    /// 清除所有已加载的媒体并将播放器模型重置为其默认状态。
    func reset() {
        currentItem = nil
        player.replaceCurrentItem(with: nil)
        playerViewController = nil
        playerViewControllerDelegate = nil
        // 在运行循环的下一个周期中重置演示文稿状态。
        Task { @MainActor in
            presentation = .inline
        }
    }

    /// 从视频项数据中创建元数据项
    /// - Parameter video: 要创建元数据的视频。
    /// - Returns: 设置在播放器项目上的 `AVMetadataItem` 数组。
    private func createMetadataItems(for video: Video) -> [AVMetadataItem] {
        let mapping: [AVMetadataIdentifier: Any] = [
            .commonIdentifierTitle: video.title,
            .commonIdentifierArtwork: video.imageData,
            .commonIdentifierDescription: video.description,
            .commonIdentifierCreationDate: video.info.releaseDate,
            .iTunesMetadataContentRating: video.info.contentRating,
            .quickTimeMetadataGenre: video.info.genres
        ]
        return mapping.compactMap { createMetadataItem(for: $0, value: $1) }
    }

    /// 为指定的标识符和值创建一个元数据项
    /// - Parameters:
    ///   - identifier: 项目的标识符
    ///   - value: 与项目关联的值
    /// - Returns: 新的 `AVMetadataItem` 对象。
    private func createMetadataItem(for identifier: AVMetadataIdentifier,
                                    value: Any) -> AVMetadataItem {
        let item = AVMutableMetadataItem()
        item.identifier = identifier
        item.value = value as? NSCopying & NSObjectProtocol
        // 指定 "und" 来表示未定义的语言。
        item.extendedLanguageTag = "und"
        return item.copy() as! AVMetadataItem
    }

    /// 配置用户的预定空音频体验，以最适应演示
    /// - Parameter presentation: 请求的播放器演示
    private func configureAudioExperience(for presentation: Presentation) {
        #if os(visionOS)
        do {
            let experience: AVAudioSessionSpatialExperience
            switch presentation {
            case .inline:
                // 当观看预告片时，设置一个小的、有聚焦效果的声音舞台
                experience = .headTracked(soundStageSize: .small, anchoringStrategy: .automatic)
            case .fullWindow:
                // 在全窗口查看时，设置大型音效舞台
                experience = .headTracked(soundStageSize: .large, anchoringStrategy: .automatic)
            }
            try AVAudioSession.sharedInstance().setIntendedSpatialExperience(experience)
        } catch {
            logger.error("无法设置预期的空间体验。 \(error.localizedDescription)")
        }
        #endif
    }

    // MARK: - Transport Control

    func play() {
        player.play()
    }

    func pause() {
        player.pause()
    }

    func togglePlayback() {
        player.timeControlStatus == .paused ? play() : pause()
    }

    // MARK: - Time Observation
    private func addTimeObserver() {
        removeTimeObserver()
        // 每1秒观察一次播放器的时间
        let timeInterval = CMTime(value: 1, timescale: 1)
        timeObserver = player.addPeriodicTimeObserver(forInterval: timeInterval, queue: .main) { [weak self] time in
            guard let self = self, let duration = player.currentItem?.duration else { return }
            // 在当前剧集结束前的10秒内提出播放下一集
            let isInProposalRange = time.seconds >= duration.seconds - 10.0
            if shouldProposeNextVideo != isInProposalRange {
                shouldProposeNextVideo = isInProposalRange
            }
        }
    }
    
    private func removeTimeObserver() {
        guard let timeObserver = timeObserver else { return }
        player.removeTimeObserver(timeObserver)
        self.timeObserver = nil
    }
    
    final class PlayerViewControllerDelegate: NSObject, AVPlayerViewControllerDelegate {
        
        let player: PlayerModel
        
        init(player: PlayerModel) {
            self.player = player
        }
        
        #if os(visionOS)
        // 当用户点击视 visionOS 玩家用户界面中的返回按钮时，重置模型的状态
        func playerViewController(_ playerViewController: AVPlayerViewController,
                                  willEndFullScreenPresentationWithAnimationCoordinator coordinator: UIViewControllerTransitionCoordinator) {
            Task { @MainActor in
                // C调用reset来关闭全屏播放器
                player.reset()
            }
        }
        #endif
    }
}
