//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import SwiftUI

/// 视频卡片支持的样式的
enum VideoCardStyle {
    
    /// 这种样式在顶部呈现一个海报图片
    /// 底部显示关于视频的信息
    /// 包括视频的描述和体裁
    case full

    /// 适用于“即将播出”列表的卡片样式
    ///
    /// 这种样式在顶部呈现一个中等大小的海报图片，底部呈现标题字符串
    case upNext
    
    /// 紧凑的视频卡片样式
    ///
    /// 这种样式在顶部呈现一个紧凑大小的海报图片，底部呈现标题字符串
    case compact
    
    var cornerRadius: Double {
        switch self {
        case .full:
            #if os(tvOS)
            12.0
            #else
            20.0
            #endif
            
        case .upNext: 12.0
        case .compact: 10.0
        }
    }

}

/// 一个代表库中视频的视图
///
/// 用户可以选择一个视频卡来查看视频的详细信息
struct VideoCardView: View {
    
    let video: Video
    let style: VideoCardStyle
    let cornerRadius = 20.0
    
    /// 用视频和可选样式创建一个视频卡视图
    ///
    /// 默认样式是`.full`
    init(video: Video, style: VideoCardStyle = .full) {
        self.video = video
        self.style = style
    }
    
    var image: some View {
        Image(video.landscapeImageName)
            .resizable()
            .scaledToFill()
    }

    var body: some View {
        switch style {
        case .compact:
            posterCard
                .frame(width: valueFor(iOS: 0, tvOS: 400, visionOS: 200))
        case .upNext:
            posterCard
                .frame(width: valueFor(iOS: 250, tvOS: 500, visionOS: 360))
        case .full:
            VStack {
                image
                VStack(alignment: .leading) {
                    InfoLineView(year: video.info.releaseYear,
                                 rating: video.info.contentRating,
                                 duration: video.info.duration)
                    .foregroundStyle(.secondary)
                    .padding(.top, -10)
                    
                    Text(video.title)
                        .font(isTV ? .title3 : .title)
                        
                    Text(video.description)
                    #if os(tvOS)
                        .font(.callout)
                    #endif
                        .lineLimit(2)
                    Spacer()
                    HStack {
                        GenreView(genres: video.info.genres)
                    }
                }
                .padding(20)
            }
            .background(.thinMaterial)
            #if os(tvOS)
            .frame(width: 550, height: 590)
            #else
            .frame(width: isVision ? 395 : 300)
            .shadow(radius: 5)
            .hoverEffect()
            #endif
            .cornerRadius(style.cornerRadius)
        }
    }
    
    @ViewBuilder
    var posterCard: some View {
        #if os(tvOS)
        ZStack(alignment: .bottom) {
            image
            // 材质渐变
            GradientView(style: .ultraThinMaterial, height: 90, startPoint: .top)
            Text(video.title)
                .font(.caption.bold())
                .padding()
        }
        .cornerRadius(style.cornerRadius)
        #else
        VStack {
            image
                .cornerRadius(style.cornerRadius)
            Text(video.title)
                .font(isVision ? .title3 : .headline)
                .lineLimit(1)
        }
        .hoverEffect()
        #endif
    }
}
