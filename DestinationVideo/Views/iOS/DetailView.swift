//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import SwiftUI

struct DetailView: View {
    
    let video: Video
    @Environment(\.horizontalSizeClass) var horizontalSizeClass
    @Environment(PlayerModel.self) private var player
    @Environment(VideoLibrary.self) private var library
    
    var body: some View {
        GeometryReader() { geometry in
            ZStack(alignment: .bottom) {
                // 背景视图和渐变色
                Group {
                    // 根据几何比例显示合适的背景图片
                    backgroundImage(for: geometry)
                    // 渐变色覆盖视图
                    gradientOverlay
                }
                .ignoresSafeArea()
                // 视频信息布局
                if horizontalSizeClass == .compact {
                    VStack {
                        Text(video.title)
                            .font(.title.weight(.semibold))
                        InfoLineView(year: video.info.releaseYear,
                                     rating: video.info.contentRating,
                                     duration: video.info.duration)
                        Button {
                            /// 加载媒体条目以全屏显示
                            player.loadVideo(video, presentation: .fullWindow)
                        } label: {
                            Label("播放视频", systemImage: "play.fill")
                                .fontWeight(.semibold)
                                .foregroundStyle(.black)
                        }
                        .buttonStyle(.borderedProminent)
                        Text(video.description)
                            .padding()
                    }
                    
                } else {
                    VStack {
                        Text(video.title)
                            .font(.largeTitle).bold()
                        // iPadOS和tvOS共享此视图来呈现视频详细信息
                        WideDetailView(video: video, player: player, library: library)
                    }
                }
            }
        }
        // 不在iOS中显示导航标题
        .navigationTitle("")
    }
    
    /// 返回适合指定几何图形的背景图片
    ///
    /// - Parameter geometry: 用于计算的几何图形代理
    /// - Returns: 适合当前几何图形的可调整大小的图片
    func backgroundImage(for geometry: GeometryProxy) -> Image {
        let usePortrait = geometry.size.height > geometry.size.width
        return Image(usePortrait ? video.portraitImageName : video.landscapeImageName).resizable()
    }
    
    var gradientOverlay: some View {
        VStack {
            // 在顶部添加一个微妙的渐变，使返回按钮更突出
            GradientView(style: .black.opacity(0.2), height: 120, startPoint: .bottom)
            Spacer()
            // 在底部添加一个材质渐变，用于显示视频的详细信息
            GradientView(style: .thinMaterial, height: 400, startPoint: .top)
        }
    }
}

#Preview {
    NavigationStack {
        DetailView(video: .preview)
            .environment(PlayerModel())
            .environment(VideoLibrary())
    }
}
