//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import SwiftUI

struct VideoInfoView: View {
    let video: Video
    var body: some View {
        VStack(alignment: .leading) {
            Text(video.title)
                .font(isVision ? .title : .title2)
                .padding(.bottom, 4)
            InfoLineView(year: video.info.releaseYear,
                         rating: video.info.contentRating,
                         duration: video.info.duration)
                .padding([.bottom], 4)
            GenreView(genres: video.info.genres)
                .padding(.bottom, 4)
            RoleView(role: String(localized: "主演"), people: video.info.stars)
                .padding(.top, 1)
            RoleView(role: String(localized: "导演"), people: video.info.directors)
                .padding(.top, 1)
            RoleView(role: String(localized: "编剧"), people: video.info.writers)
                .padding(.top, 1)
                .padding(.bottom, 12)
            Text(video.description)
                .font(.headline)
                .padding(.bottom, 12)
        }
    }
}

/// 显示水平列表，包含视频的年份、评级和时长
struct InfoLineView: View {
    let year: String
    let rating: String
    let duration: String
    var body: some View {
        HStack {
            Text("\(year) | \(rating) | \(duration)")
                .font(isTV ? .caption : .subheadline.weight(.medium))
        }
    }
}

/// 显示视频的体裁的视图，为这个视频显示一个逗号分隔的体裁列表
struct GenreView: View {
    let genres: [String]
    var body: some View {
        HStack(spacing: 8) {
            ForEach(genres, id: \.self) {
                Text($0)
                    .fixedSize()
                #if os(visionOS)
                    .font(.caption2.weight(.bold))
                #else
                    .font(.caption)
                #endif
                    .padding([.leading, .trailing], isTV ? 8: 4)
                    .padding([.top, .bottom], 4)
                    .background(RoundedRectangle(cornerRadius: 5).stroke())
                    .foregroundStyle(.secondary)
            }
            // 将列表推到leading
            Spacer()
        }
    }
}

/// 显示角色名称的视图
struct RoleView: View {
    let role: String
    let people: [String]
    var body: some View {
        VStack(alignment: .leading) {
            Text(role)
            Text(people.formatted())
                .foregroundStyle(.secondary)
        }
    }
}

#if os(visionOS)
#Preview {
    VideoInfoView(video: .preview)
        .padding()
        .frame(width: 500, height: 500)
        .background(.gray)
        .previewLayout(.sizeThatFits)
}
#endif
