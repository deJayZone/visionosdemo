//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import SwiftUI

struct LibraryView: View {
    
    @Environment(PlayerModel.self) private var model
    @Environment(VideoLibrary.self) private var library
    
    /// 内容导航路径
    @Binding private var navigationPath: [Video]
    /// 是否可以出现空间
    @Binding private var isPresentingSpace: Bool
    
    /// 使用绑定到选定路径的方式创建一个'LibraryView'
    ///
    /// 默认值是一个空的绑定
    init(path: Binding<[Video]>, isPresentingSpace: Binding<Bool> = .constant(false)) {
        _navigationPath = path
        _isPresentingSpace = isPresentingSpace
    }
    
    var body: some View {
        NavigationStack(path: $navigationPath) {
            // 把内容放在竖直滚动视图中
            ScrollView(showsIndicators: false) {
                VStack(alignment: .leading, spacing: verticalPadding) {
                    // 显示Destination Video的Logo图像
                    Image("dv_logo")
                        .resizable()
                        .scaledToFit()
                        .padding(.leading, outerPadding)
                        .padding(.bottom, isMobile ? 0 : 8)
                        .frame(height: logoHeight)
                        .accessibilityHidden(true)
                    
                    // 显示一列水平滚动的精选视频
                    VideoListView(title: "精选",
                                  videos: library.videos,
                                  cardStyle: .full,
                                  cardSpacing: horizontalSpacing)
                    
                    // 显示“即将播放”的视频列表。
                    VideoListView(title: "即将播放",
                                  videos: library.upNext,
                                  cardStyle: .upNext,
                                  cardSpacing: horizontalSpacing)
                }
                .padding([.top, .bottom], verticalPadding)
                .navigationDestination(for: Video.self) { video in
                    DetailView(video: video)
                        .navigationTitle(video.title)
                        .navigationBarHidden(isTV)
                }
            }
            #if os(tvOS)
            .ignoresSafeArea()
            #endif
        }
        #if os(visionOS)
        // 自定义视图修改器，当导航到详细视图时呈现沉浸式空间
        .updateImmersionOnChange(of: $navigationPath, isPresentingSpace: $isPresentingSpace)
        #endif
    }

    // MARK: - 平台适配
    /// 视图之间的垂直填充
    var verticalPadding: Double {
        valueFor(iOS: 30, tvOS: 40, visionOS: 30)
    }
    
    var outerPadding: Double {
        valueFor(iOS: 20, tvOS: 50, visionOS: 30)
    }
    
    var horizontalSpacing: Double {
        valueFor(iOS: 20, tvOS: 80, visionOS: 30)
    }
    
    var logoHeight: Double {
        valueFor(iOS: 24, tvOS: 60, visionOS: 34)
    }
}

#Preview {
    NavigationStack {
        LibraryView(path: .constant([]))
            .environment(PlayerModel())
            .environment(VideoLibrary())
    }
}
