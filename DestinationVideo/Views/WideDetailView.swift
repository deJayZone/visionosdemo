//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import SwiftUI

/// 一个以水平布局展示操作控制和视频详情的视图
///
/// 这个视图在iPadOS和tvOS的详细界面中被用来展示视频信息
struct WideDetailView: View {
    
    let video: Video
    let player: PlayerModel
    let library: VideoLibrary
    
    var body: some View {
        // 水平布局
        HStack(alignment: .top, spacing: isTV ? 40 : 20) {
            VStack {
                Button {
                    /// 加载媒体用于全屏播放
                    player.loadVideo(video, presentation: .fullWindow)
                } label: {
                    Label("播放视频", systemImage: "play.fill")
                        .frame(maxWidth: .infinity)
                }
                
                Button {
                    // 切换视频在“即将播放”队列中的状态
                    library.toggleUpNextState(for: video)
                } label: {
                    let isUpNext = library.isVideoInUpNext(video)
                    Label(isUpNext ? "在即将播放中" : "添加到即将播放",
                          systemImage: isUpNext ? "checkmark" : "plus")
                            .frame(maxWidth: .infinity)

                }
            }
            .fontWeight(.semibold)
            .foregroundStyle(.black)
            .buttonStyle(.borderedProminent)
            .frame(width: isTV ? 400 : 200)
            .fixedSize(horizontal: true, vertical: false)
            
            Text(video.description)
            
            VStack(alignment: .leading, spacing: 4) {
                RoleView(role: "主演", people: video.info.stars)
                RoleView(role: "导演", people: video.info.directors)
                RoleView(role: "编剧", people: video.info.writers)
            }
            
        }
        .frame(height: isTV ? 300 : 150)
        .padding([.leading, .trailing], isTV ? 80 : 40)
        .padding(.bottom, 20)
    }
}

