//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import SwiftUI

// 此视图左侧显示预告片，右侧显示视频信息及操作控制按钮
struct DetailView: View {
    
    let video: Video
    @Environment(PlayerModel.self) private var player
    @Environment(VideoLibrary.self) private var library

    let margins = 30.0

    var body: some View {
        HStack(alignment: .top, spacing: margins) {
            // 嵌入展示的视频播放视图
            TrailerView(video: video)
                .aspectRatio(16 / 9, contentMode: .fit)
                .frame(width: 620)
                .cornerRadius(20)
            
            VStack(alignment: .leading) {
                // 显示视频详情
                VideoInfoView(video: video)
                // 操作控制
                HStack {
                    Group {
                        Button {
                            /// 加载全窗口展示的媒体
                            player.loadVideo(video, presentation: .fullWindow)
                        } label: {
                            Label("播放视频", systemImage: "play.fill")
                                .frame(maxWidth: .infinity)
                        }
                        Button {
                            // 调用此方法将视频加入或移出“接下来播放”队列
                            library.toggleUpNextState(for: video)
                        } label: {
                            let isUpNext = library.isVideoInUpNext(video)
                            Label(isUpNext ? "已加入播放列表" : "加入播放列表",
                                  systemImage: isUpNext ? "checkmark" : "plus")
                            .frame(maxWidth: .infinity)
                        }
                    }
                    .frame(maxWidth: .infinity)
                }
                .frame(maxWidth: 420)
                Spacer()
            }
        }
        .padding(margins)
    }
}

#Preview {
    NavigationStack {
        DetailView(video: .preview)
            .environment(PlayerModel())
            .environment(VideoLibrary())
    }
}
