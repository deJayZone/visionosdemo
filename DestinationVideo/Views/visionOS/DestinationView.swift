//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import SwiftUI
import RealityKit
import Combine

/// 显示一个360度场景的视图，可以在其中观看视频
struct DestinationView: View {
    
    @State private var destination: Destination
    @State private var destinationChanged = false
    
    @Environment(PlayerModel.self) private var model
    
    init(_ destination: Destination) {
        self.destination = destination
    }
    
    var body: some View {
        RealityView { content in
            let rootEntity = Entity()
            rootEntity.addSkybox(for: destination)
            content.add(rootEntity)
        } update: { content in
            guard destinationChanged else { return }
            guard let entity = content.entities.first else { fatalError() }
            entity.updateTexture(for: destination)
            Task { @MainActor in
                destinationChanged = false
            }
        }
        // 处理当应用程序已经在播放视频，并且：
        // 1. 用户打开"即将播放"标签并导航到新项目，或
        // 2. 用户在播放器UI中按下"播放下一个"按钮
        .onChange(of: model.currentItem) { oldValue, newValue in
            if let newValue, destination != newValue.destination {
                destination = newValue.destination
                destinationChanged = true
            }
        }
        .transition(.opacity)
    }
}

extension Entity {
    func addSkybox(for destination: Destination) {
        let subscription = TextureResource.loadAsync(named: destination.imageName).sink(
            receiveCompletion: {
                switch $0 {
                case .finished: break
                case .failure(let error): assertionFailure("\(error)")
                }
            },
            receiveValue: { [weak self] texture in
                guard let self = self else { return }
                var material = UnlitMaterial()
                material.color = .init(texture: .init(texture))
                self.components.set(ModelComponent(
                    mesh: .generateSphere(radius: 1E3),
                    materials: [material]
                ))
                self.scale *= .init(x: -1, y: 1, z: 1)
                self.transform.translation += SIMD3<Float>(0.0, 1.0, 0.0)
                
                // 旋转球体以显示空间的最佳初始视图
                updateRotation(for: destination)
            }
        )
        components.set(Entity.SubscriptionComponent(subscription: subscription))
    }
    
    func updateTexture(for destination: Destination) {
        let subscription = TextureResource.loadAsync(named: destination.imageName).sink(
            receiveCompletion: {
                switch $0 {
                case .finished: break
                case .failure(let error): assertionFailure("\(error)")
                }
            },
            receiveValue: { [weak self] texture in
                guard let self = self else { return }
                
                guard var modelComponent = self.components[ModelComponent.self] else {
                    fatalError("Should this be fatal? Probably.")
                }
                
                var material = UnlitMaterial()
                material.color = .init(texture: .init(texture))
                modelComponent.materials = [material]
                self.components.set(modelComponent)
                
                // 旋转球体以显示空间的最佳初始视图
                updateRotation(for: destination)
            }
        )
        components.set(Entity.SubscriptionComponent(subscription: subscription))
    }
    
    func updateRotation(for destination: Destination) {
        // 围绕Y轴旋转沉浸空间来设置用户的沉浸式场景的初始视野
        let angle = Angle.degrees(destination.rotationDegrees)
        let rotation = simd_quatf(angle: Float(angle.radians), axis: SIMD3<Float>(0, 1, 0))
        self.transform.rotation = rotation
    }
    
    /// 异步纹理加载返回的订阅的容器
    ///
    /// 为了使异步加载回调起作用，我们需要将订阅存储在某个地方。将其存储在组件上将使订阅保持活动状态，只要该组件保持连接状态
    struct SubscriptionComponent: Component {
        var subscription: AnyCancellable
    }
}
