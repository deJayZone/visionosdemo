//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import SwiftUI

/// 一个用于呈现水平滚动视频卡片列表的视图
struct VideoListView: View {
    
    typealias SelectionAction = (Video) -> Void

    private let title: String?
    private let videos: [Video]
    private let cardStyle: VideoCardStyle
    private let cardSpacing: Double

    private let selectionAction: SelectionAction?
    
    /// 创建一个用于显示指定视频列表的视图。
    /// - Parameters:
    ///   - title: 列表上方可选的标题.
    ///   - videos: 要显示的视频列表.
    ///   - cardStyle: 视频卡的样式.
    ///   - cardSpacing: 卡片之间的间距.
    ///   - selectionAction: 你可以指定的直接处理卡片选择的可选操作。
    ///     当应用程序没有指定选择动作时，视图将卡片作为`NavigationLink`呈现
    init(title: String? = nil, videos: [Video], cardStyle: VideoCardStyle, cardSpacing: Double, selectionAction: SelectionAction? = nil) {
        self.title = title
        self.videos = videos
        self.cardStyle = cardStyle
        self.cardSpacing = cardSpacing
        self.selectionAction = selectionAction
    }
    
    var body: some View {
        VStack(alignment: .leading, spacing: 0) {
            titleView
                .padding(.leading, margins)
                .padding(.bottom, valueFor(iOS: 8, tvOS: -40, visionOS: 12))
            ScrollView(.horizontal, showsIndicators: false) {
                HStack(spacing: cardSpacing) {
                    ForEach(videos) { video in
                        Group {
                            // 如果应用程序使用选择操作闭包初始化视图
                            // 显示一个调用的视频卡片按钮
                            if let selectionAction {
                                Button {
                                    selectionAction(video)
                                } label: {
                                    VideoCardView(video: video, style: cardStyle)
                                }
                            }
                            // 否则创建一个 NavigationLink
                            else {
                                NavigationLink(value: video) {
                                    VideoCardView(video: video, style: cardStyle)
                                }
                            }
                        }
                        .accessibilityLabel("\(video.title)")
                    }
                }
                .buttonStyle(buttonStyle)
                // 在tvOS中，添加垂直填充以适应卡片调整大小
                .padding([.top, .bottom], isTV ? 60 : 0)
                .padding([.leading, .trailing], margins)
            }
        }
    }
    
    @ViewBuilder
    var titleView: some View {
        if let title {
            Text(title)
            #if os(visionOS)
                .font(cardStyle == .full ? .largeTitle : .title)
            #elseif os(tvOS)
                .font(cardStyle == .full ? .largeTitle.weight(.semibold) : .title2)
            #else
                .font(cardStyle == .full ? .title2.bold() : .title3.bold())
            #endif
            
        }
    }
    
    var buttonStyle: some PrimitiveButtonStyle {
        #if os(tvOS)
        .card
        #else
        .plain
        #endif
    }
    
    var margins: Double {
        valueFor(iOS: 20, tvOS: 50, visionOS: 30)
    }
}

#Preview("Full") {
    NavigationStack {
        VideoListView(title: "精选", videos: .all, cardStyle: .full, cardSpacing: 80)
            .frame(height: 380)
    }
}

#Preview("Up Next") {
    NavigationStack {
        VideoListView(title: "下一个", videos: .all, cardStyle: .upNext, cardSpacing: 20)
            .frame(height: 200)
    }
}

#Preview("Compact") {
    NavigationStack {
        VideoListView(videos: .all, cardStyle: .compact, cardSpacing: 20)
            .padding()
    }
}
