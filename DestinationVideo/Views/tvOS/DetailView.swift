//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import SwiftUI

struct DetailView: View {
    
    let video: Video
    @Environment(PlayerModel.self) private var player
    @Environment(VideoLibrary.self) private var library
    
    var body: some View {
        ZStack(alignment: .bottom) {
            // 背景图像
            Image(video.landscapeImageName)
                .resizable()
                .scaledToFill()
            // 材质渐变
            GradientView(style: .thinMaterial, height: 450, startPoint: .top)
            // 视频内容
            VStack {
                HStack {
                    Text(video.title)
                        .font(.system(size: 96, weight: .bold, design: .rounded))
                        .shadow(radius: 10)
                        .padding([.leading, .top], 100)
                    Spacer()
                }
                Spacer()
                WideDetailView(video: video, player: player, library: library)
            }
        }
        .ignoresSafeArea()
    }
}

#Preview {
    NavigationStack {
        DetailView(video: .preview)
            .environment(PlayerModel())
            .environment(VideoLibrary())
    }
}
