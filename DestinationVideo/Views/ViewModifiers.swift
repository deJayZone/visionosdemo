//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import SwiftUI

extension View {
    #if os(visionOS)
    func updateImmersionOnChange(of path: Binding<[Video]>, isPresentingSpace: Binding<Bool>) -> some View {
        self.modifier(ImmersiveSpacePresentationModifier(navigationPath: path, isPresentingSpace: isPresentingSpace))
    }
    #endif
    
    // 仅在iOS和tvOS上用于全屏模式对话框的显示
    func fullScreenCoverPlayer(player: PlayerModel) -> some View {
        self.modifier(FullScreenCoverModifier(player: player))
    }
}

#if os(visionOS)
private struct ImmersiveSpacePresentationModifier: ViewModifier {
    
    @Environment(\.openImmersiveSpace) private var openSpace
    @Environment(\.dismissImmersiveSpace) private var dismissSpace
    /// 场景的当前阶段，可以是active，inactive或background
    @Environment(\.scenePhase) private var scenePhase
    
    @Binding var navigationPath: [Video]
    @Binding var isPresentingSpace: Bool
    
    func body(content: Content) -> some View {
        content
            .onChange(of: navigationPath) {
                Task {
                    // 当用户返回到主库窗口时，选择路径将变为空
                    if navigationPath.isEmpty {
                        if isPresentingSpace {
                            // 关闭空间并将用户返回到他们的真实世界空间
                            await dismissSpace()
                            isPresentingSpace = false
                        }
                    } else {
                        guard !isPresentingSpace else { return }
                        // 导航路径只有一个视频，或者为空
                        guard let video = navigationPath.first else { fatalError() }
                        // Await the request to open the destination and set the state accordingly.
                        switch await openSpace(value: video.destination) {
                        case .opened: isPresentingSpace = true
                        default: isPresentingSpace = false
                        }
                    }
                }
            }
            // 当用户将应用放入后台时，关闭空间并卸载媒体
            .onChange(of: scenePhase) { _, newPhase in
                if isPresentingSpace, newPhase == .background {
                    Task {
                        await dismissSpace()
                    }
                }
            }
    }
}
#endif

private struct FullScreenCoverModifier: ViewModifier {
    
    let player: PlayerModel
    @State private var isPresentingPlayer = false
    
    func body(content: Content) -> some View {
        content
            .fullScreenCover(isPresented: $isPresentingPlayer) {
                PlayerView()
                    .onAppear {
                        player.play()
                    }
                    .onDisappear {
                        player.reset()
                    }
                    .frame(maxWidth: .infinity, maxHeight: .infinity)
                    .ignoresSafeArea()
            }
            // 观察播放器的显示属性
            .onChange(of: player.presentation, { _, newPresentation in
                isPresentingPlayer = newPresentation == .fullWindow
            })
    }
}

