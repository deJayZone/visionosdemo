//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import SwiftUI

/// 垂直渐变视图
struct GradientView: View {
    
    /// 渐变填充样式
    let style: any ShapeStyle
    
    /// 视图高度
    let height: Double
    
    /// 渐变的起点
    ///
    /// 这个值可以是`.top`或.`bottom`
    let startPoint: UnitPoint
    
    /// 创建一个渐变视图。
    /// - 参数：
    ///   - style: 渐变的填充样式。
    ///   - height: 视图的高度，单位是点。
    ///   - startPoint: 渐变的起点。如果该值
    ///   不是`.top`或`.bottom`，系统会抛出错误。
    init(style: any ShapeStyle, height: Double, startPoint: UnitPoint) {
        guard startPoint == .top || startPoint == .bottom else { fatalError() }
        self.style = style
        self.height = height
        self.startPoint = startPoint
    }
    
    var body: some View {
        Rectangle()
            .fill(AnyShapeStyle(style))
            .frame(height: height)
            .mask {
                LinearGradient(colors: [.clear, .black, .black],
                               startPoint: startPoint,
                               // 设置终点是起点的对立
                               endPoint: startPoint == .top ? .bottom : .top)
            }
    }
}

#Preview {
    GradientView(style: .thinMaterial, height: 200, startPoint: .top)
}
