//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import Foundation
import Combine
import GroupActivities
import CoreTransferable
import UIKit

struct VideoWatchingActivity: GroupActivity, Transferable {
    
    // 要观看的视频
    let video: Video
    
    // 系统向参与者展示的元数据
    var metadata: GroupActivityMetadata {
        var metadata = GroupActivityMetadata()
        //活动类型为 “一起观看”
        metadata.type = .watchTogether
        // 视频标题
        metadata.title = video.title
        // 视频预览图像
        metadata.previewImage = previewImage
        // 如果无法创建活动则使用后备 URL 来查看内容。
        metadata.fallbackURL = fallbackURL
        return metadata
    }
    
    var previewImage: CGImage? {
        UIImage(named: video.landscapeImageName)?.cgImage
    }
    
    var fallbackURL: URL? {
        // 如果处理远程媒体，则将媒体的URL指定为后备
        video.hasRemoteMedia ? video.resolvedURL : nil
    }
}
