//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import Foundation
import Combine
import GroupActivities
import AVFoundation

actor VideoWatchingCoordinator {
    
    @Published private(set) var sharedVideo: Video?
    
    private var subscriptions = Set<AnyCancellable>()
    
    private let coordinatorDelegate = CoordinatorDelegate()
    
    private var playbackCoordinator: AVPlayerPlaybackCoordinator
    
    init(playbackCoordinator: AVPlayerPlaybackCoordinator) {
        self.playbackCoordinator = playbackCoordinator
        self.playbackCoordinator.delegate = coordinatorDelegate
        Task {
            await startObservingSessions()
        }
    }
    
    private func startObservingSessions() async {
        // 等待新的会话以一起观看视频
        for await session in VideoWatchingActivity.sessions() {
            
            // 如果存在旧的会话，先清理
            cleanUpSession(groupSession)
            
            #if os(visionOS)
            // 检索新会话的系统协调器对象以更新其配置
            guard let systemCoordinator = await session.systemCoordinator else { continue }
            // 创建一种新的配置，使所有参与者能够共享同一的沉浸式空间
            var configuration = SystemCoordinator.Configuration()
            configuration.supportsGroupImmersiveSpace = true
            systemCoordinator.configuration = configuration
            #endif
            
            // 设置这个应用的活跃群组会话，然后再加入
            groupSession = session
            
            let stateListener = Task {
                await self.handleStateChanges(groupSession: session)
            }
            subscriptions.insert(.init { stateListener.cancel() })
            
            // 观察当本地用户或远程参与者更改 GroupSession 上的活动
            let activityListener = Task {
                await self.handleActivityChanges(groupSession: session)
            }
            subscriptions.insert(.init { activityListener.cancel() })
            
            // 加入会话
            session.join()
        }
    }

    private func cleanUpSession(_ session: GroupSession<VideoWatchingActivity>?) {
        // 如果会议并不是播放器模型会话的相同实例，则提前退出
        guard groupSession === session else { return }
        // 离开会话并将会话和视频设置为nil以发布无效状态
        groupSession?.leave()
        groupSession = nil
        sharedVideo = nil
        
        subscriptions.removeAll()
    }

    private var groupSession: GroupSession<VideoWatchingActivity>? {
        didSet {
            guard let groupSession = groupSession else { return }
            // 将组会话设置在AVPlayer实例的播放协调器上，以便能与其他设备同步播放。
            playbackCoordinator.coordinateWithSession(groupSession)
        }
    }

    private func handleActivityChanges(groupSession: GroupSession<VideoWatchingActivity>) async {
        for await newActivity in groupSession.$activity.values {
            guard groupSession === self.groupSession else { return }
            updateSharedVideo(video: newActivity.video)
        }
    }

    private func handleStateChanges(groupSession: GroupSession<VideoWatchingActivity>) async {
        for await newState in groupSession.$state.values {
            if case .invalidated = newState {
                cleanUpSession(groupSession)
            }
        }
    }

    /// 更新这个actor的`sharedVideo`状态
    /// - Parameter video: 设为共享的视频。
    private func updateSharedVideo(video: Video) {
        coordinatorDelegate.video = video
        // 将视频设置为共享视频。
        sharedVideo = video
    }

    /// 使用其他人在组会话中协调播放此视频
    /// - Parameter video: 要分享的视频
    func coordinatePlayback(of video: Video) async {
        // 如果此视频已经共享，则提前退出
        guard video != sharedVideo else { return }
        
        // 为选定的视频创建新的活动
        let activity = VideoWatchingActivity(video: video)
        
        switch await activity.prepareForActivation() {
        case .activationPreferred:
            do {
                // 尝试激活新的活动
                _ = try await activity.activate()
            } catch {
                logger.debug("无法激活活动: \(error)")
            }
        case .activationDisabled:
            // FaceTime会话未激活，或用户希望在小组之外播放视频。将sharedVideo设置为nil。
            sharedVideo = nil
        default:
            break
        }
    }

    /// 实现`AVPlayerPlaybackCoordinatorDelegate`的一个类，该类决定了
    /// 播放协调器如何识别本地和远程媒体。
    private class CoordinatorDelegate: NSObject, AVPlayerPlaybackCoordinatorDelegate {
        var video: Video?
        // 当播放本地媒体时，需要采用这种方法，
        // 或者在任何需要自定义媒体识别策略的情况下。
        // 如果未实现此方法，协调播放将不能正确地运行。
        func playbackCoordinator(_ coordinator: AVPlayerPlaybackCoordinator,
                                 identifierFor playerItem: AVPlayerItem) -> String {
            // 返回视频id作为播放项标识符。
            "\(video?.id ?? -1)"
        }
    }
}

