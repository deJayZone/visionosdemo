//
//  DestinationVideo
//
//  Created by dejayzone on 2024/2/29.
//  Copyright © 2024 Apple. All rights reserved.
//

import SwiftUI
import os

@main
struct DestinationVideo: App {
    
    /// 控制视频播放行为的对象
    @State private var player = PlayerModel()
    /// 管理视频内容库的对象
    @State private var library = VideoLibrary()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(player)
                .environment(library)
            #if !os(visionOS)
                .preferredColorScheme(.dark)
                .tint(.white)
            #endif
        }
        #if os(visionOS)
        // 定义一个沉浸式空间来呈现视频
        ImmersiveSpace(for: Destination.self) { $destination in
            if let destination {
                DestinationView(destination)
                    .environment(player)
            }
        }
        // 将沉浸式样式设置为渐进式，真机可以使用表冠调整
        .immersionStyle(selection: .constant(.progressive), in: .progressive)
        #endif
    }
}

/// 全局日志记录器
let logger = Logger()
